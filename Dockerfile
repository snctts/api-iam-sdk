FROM node:10.17.0-alpine

ARG WORK_DIR=/var/www/node
RUN mkdir -p ${WORK_DIR}
COPY . ${WORK_DIR}/
WORKDIR ${WORK_DIR}

RUN apk update && apk upgrade && apk add --no-cache bash git
RUN npm install
RUN npm run build
RUN rm -fr node_modules
RUN rm -fr src
RUN npm install --production
RUN rm -f .npmrc

