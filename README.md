# api-iam-sdk
## Description

This lib allow you to collect all policies from your service marked with decorator `@Auth` and send it to `api-iam` service 
to validate requests to your service with `api-auth` in future.

## How to use

For now you can use lib from develop branch.
First of all you need to install it to your service with

After that you have to install `@golevelup/nestjs-discovery` lib for parsing decorator for the whole main module

```
npm install --save @golevelup/nestjs-discovery
```

Also you will need class `Reflector` from `@nestjs/core`.

So, now your imports in main module(for example `app.module.ts`) can looks like

```
...
import {DiscoveryModule, DiscoveryService} from '@golevelup/nestjs-discovery';
import {Reflector} from '@nestjs/core';
import {IamSdk} from 'api-iam-sdk';
...
```

In next step you have to add `DiscoveryModule` to the module imports section. 
```
@Module({
  imports: [
    ...
    DiscoveryModule,
  ],
})
```

For using `@golevelup/nestjs-discovery` lib in a right way it should be used only inside `OnModuleInit` method.
So, your module should implement it and `api-iam-sdk` will be initialized inside `OnModuleInit`.

```
export class ApplicationModule implements NestModule, OnModuleInit {

  constructor(private readonly discovery: DiscoveryService, private reflector: Reflector) {}

  public async onModuleInit() {
    const sdk = new IamSdk(this.discovery, this.reflector, {
      logProcess: true,
      baseUrl: config.get('service.baseUrl'),
      serviceName: config.get('service.name'),
      iamHost: config.get('services.apiIam.host'),
    });

    await sdk.updateEndpointPolicies();
  }
}
```

In a constructor `api-iam-sdk` has 3 parameters:
- Dicscovery service instance (`discovery`)
- Reflector instance (`reflector`)
- Settings object
And 2 optional parameters:
- setEndpointsDataCb - callback method for setting policies data
- checkHashSumCb - callback method for getting checksum


Settings object has interface:
```
{
  logProcess?: boolean;  // Does debug logs enabled
  baseUrl: string;       // base url of your service
  serviceName: string;   // your service name (like `api-vouchers`)   
  iamHost: string;       // api-iam host 
}
```
After you initialize object of IamSdk class you should only call method `updateEndpointPolicies`.
Note that actually you can call it without `await`.

Also take into account that endpoints in your controlles should be marked with `@Auth` decorator.

It also important to implement this decorator with setting meta key `auth` like this one 

```
import {SetMetadata} from '@nestjs/common';

export const Auth = (...args: any[]) => SetMetadata('auth', args);
```

And actual marking in controller should be looking like this:

```
  @Get(':code')
  @Roles(VouchersAdminValidationRole.READ)
  @ApiBearerAuth()
  @Auth([
    {
      namespace: 'Savis-admins',
      permissions: [VouchersAdminValidationRole.READ],
    },
  ])
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'OK',
    type: VouchersInfoDto,
  })
  @ApiResponse(ErrorResponses.internalError())
  @ApiResponse(ErrorResponses.unauthorized())
  @ApiOperation({
    operationId: 'indexVoucherInfoByCode',
    description: 'Get Vouchers info by code',
  })
  async indexVoucherInfoByCode(@Param('code') code: string): Promise<VouchersInfoDto> {
    return this.vouchersService.indexVoucherInfoByCode(code);
  }
```
You can use decorator by importing it from library

As example you can see branch `feature/ra-cn-78-permissions-configuration-deployment` on `api-vouchers`

### Additional option

Also you can use callback function for getting checksum and setting policies data.

It might be looking like this

```
...
const sdk = new IamSdk(this.discovery, this.reflector, {
      logProcess: true,
      baseUrl: config.get('service.baseUrl'),
      serviceName: config.get('service.name'),
      iamHost: config.get('services.apiIam.host'),
    }, setEndpointsDataCb, checkHashSumCb);
...
```

## Naming hints according to convention

### Namespaces

Savis mobile applications - `Savis-customers`
Admin portal - `Savis-admins`
Savis dashboards - `Savis-merchants` (clients: Savis-dashboard-client , Savis-terminal-client, Savis-concierge-portal-client)

### Permissions

- `public` permission should be used for those tokens which are designed to be used w/o `access_token`
- `${serviceName}_service_user_access` permission should be used for endpoint designed to be consumed by Savis customers within Savis Mobile application
- `${serviceName}_service_backend_access` permission should be used to protect endpoints designed for inter-service communication against external requests (e.g.: from swagger doc)