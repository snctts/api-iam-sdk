import { AxiosError } from 'axios';
export declare class Logger {
    private readonly loggerEnabled;
    constructor(loggerEnabled?: boolean);
    debug(message: string): void;
    error(error: unknown): void;
    httpError(error: AxiosError): void;
    private tryParseJsonString;
    private simplifyAxiosError;
}
