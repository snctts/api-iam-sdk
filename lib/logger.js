"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Logger = void 0;
class Logger {
    constructor(loggerEnabled = false) {
        this.loggerEnabled = loggerEnabled;
    }
    debug(message) {
        if (!this.loggerEnabled) {
            return;
        }
        // tslint:disable-next-line: no-console
        console.log(message);
    }
    error(error) {
        // tslint:disable-next-line: no-console
        console.error(error);
    }
    httpError(error) {
        // tslint:disable-next-line: no-console
        console.log(this.simplifyAxiosError(error));
    }
    tryParseJsonString(value) {
        try {
            const jsonObj = JSON.parse(value);
            return jsonObj;
        }
        catch (e) {
            return value;
        }
    }
    simplifyAxiosError(err) {
        const res = err.response;
        const _a = (res === null || res === void 0 ? void 0 : res.data) ? this.tryParseJsonString(res.data) : res === null || res === void 0 ? void 0 : res.data, { stackTrace } = _a, body = __rest(_a, ["stackTrace"]);
        return {
            method: (res === null || res === void 0 ? void 0 : res.config) && res.config.method,
            url: (res === null || res === void 0 ? void 0 : res.config) && res.config.url,
            status: res === null || res === void 0 ? void 0 : res.status,
            message: err.message ? err.message : undefined,
            body,
        };
    }
}
exports.Logger = Logger;
//# sourceMappingURL=logger.js.map