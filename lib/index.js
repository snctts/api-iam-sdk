"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Auth = exports.IamSdk = void 0;
const iamSdk_1 = require("./iamSdk/iamSdk");
Object.defineProperty(exports, "IamSdk", { enumerable: true, get: function () { return iamSdk_1.IamSdk; } });
const auth_decorator_1 = require("./auth.decorator");
Object.defineProperty(exports, "Auth", { enumerable: true, get: function () { return auth_decorator_1.Auth; } });
//# sourceMappingURL=index.js.map