"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.IamSdk = void 0;
const common_1 = require("@nestjs/common");
const crypto_1 = require("crypto");
const axios_1 = require("axios");
const iamSdk_service_1 = require("./iamSdk.service");
const constants_1 = require("../constants");
const logger_1 = require("../logger");
class IamSdk {
    constructor(discover, reflector, settings, setEndpointsDataCb, checkHashSumCb) {
        this.discover = discover;
        this.reflector = reflector;
        this.settings = settings;
        this.setEndpointsDataCb = setEndpointsDataCb;
        this.checkHashSumCb = checkHashSumCb;
        this.policiesData = {};
        this.discover = discover;
        this.logger = new logger_1.Logger(settings.logProcess);
        this.reflector = reflector;
        this.settings = settings;
        this.iamSdkService = new iamSdk_service_1.IamSdkService(this.settings.serviceName, this.settings.iamHost, this.logger, axios_1.default.create());
    }
    updateEndpointPolicies() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.collectPolicies();
            yield this.processData();
        });
    }
    collectPolicies() {
        var _a, _b, _c, _d, _e;
        return __awaiter(this, void 0, void 0, function* () {
            const decoratedMethods = yield this.discover.methodsAndControllerMethodsWithMetaAtKey(constants_1.DECORATOR);
            this.logger.debug(`Starting collection...`);
            this.logger.debug(`Total decorated methods found: ${decoratedMethods.length}`);
            for (const item of decoratedMethods) {
                const controllerRoute = this.reflector.get('path', (_b = (_a = item.discoveredMethod) === null || _a === void 0 ? void 0 : _a.parentClass) === null || _b === void 0 ? void 0 : _b.injectType);
                const endpointRoute = this.reflector.get('path', (_c = item.discoveredMethod) === null || _c === void 0 ? void 0 : _c.handler);
                const endpointMethod = this.reflector.get('method', (_d = item.discoveredMethod) === null || _d === void 0 ? void 0 : _d.handler);
                const endpointOperation = this.reflector.get('swagger/apiOperation', (_e = item.discoveredMethod) === null || _e === void 0 ? void 0 : _e.handler);
                const url = this.getUrl(controllerRoute, endpointRoute);
                item.meta[0].forEach((element) => {
                    if (!this.policiesData[element.namespace]) {
                        this.policiesData[element.namespace] = [];
                    }
                    this.logger.debug(`Url: ${url}`);
                    const policyObject = {
                        permanent: false,
                        permissions: element.permissions,
                        url,
                        method: common_1.RequestMethod[endpointMethod],
                        title: endpointOperation.operationId,
                        description: endpointOperation.description,
                    };
                    this.policiesData[element.namespace].push(policyObject);
                });
            }
            this.logger.debug(`Existing namespaces: ${Object.keys(this.policiesData)}`);
        });
    }
    getUrl(controllerRoute, endpointRoute) {
        const resultUrl = [this.settings.baseUrl];
        if (controllerRoute && controllerRoute.trim() !== '/') {
            resultUrl.push(controllerRoute.trim());
        }
        if (endpointRoute && endpointRoute.trim() !== '/') {
            resultUrl.push(endpointRoute.trim());
        }
        return resultUrl.join('/');
    }
    sortEndpointPoliciesData(data) {
        data.forEach((item) => item.permissions.sort());
        return data.sort((a, b) => {
            if (a.url < b.url) {
                return -1;
            }
            if (a.url > b.url) {
                return 1;
            }
            if (a.method < b.method) {
                return -1;
            }
            if (a.method > b.method) {
                return 1;
            }
            return 0;
        });
    }
    checkHashSum(namespace) {
        return __awaiter(this, void 0, void 0, function* () {
            let controlHashSum;
            if (this.checkHashSumCb) {
                controlHashSum = yield this.checkHashSumCb(namespace, this.settings.serviceName);
            }
            else {
                controlHashSum = yield this.iamSdkService.getControlHashSum(namespace);
            }
            const endpoints = this.policiesData[namespace];
            const sortedEndpoints = this.sortEndpointPoliciesData(endpoints);
            const hash = crypto_1.createHash('md5').update(JSON.stringify(sortedEndpoints)).digest('hex');
            this.logger.debug(`Control hashsum: ${controlHashSum}`);
            this.logger.debug(`Current hashsum: ${hash}`);
            return hash === controlHashSum;
        });
    }
    processData() {
        return __awaiter(this, void 0, void 0, function* () {
            const namespaces = Object.keys(this.policiesData);
            for (const namespace of namespaces) {
                const isDataActual = yield this.checkHashSum(namespace);
                if (!isDataActual) {
                    this.logger.debug(`Processing data for namespace: ${namespace}`);
                    if (this.setEndpointsDataCb) {
                        yield this.setEndpointsDataCb(namespace, this.settings.serviceName, this.policiesData[namespace]);
                        continue;
                    }
                    yield this.iamSdkService.sendEndpointsData(this.policiesData[namespace], namespace);
                }
            }
        });
    }
}
exports.IamSdk = IamSdk;
//# sourceMappingURL=iamSdk.js.map