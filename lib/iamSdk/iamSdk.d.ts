import { DiscoveryService } from '@golevelup/nestjs-discovery';
import { Reflector } from '@nestjs/core';
import { IGlobalSetting } from './iamSdk.interface';
export declare class IamSdk {
    readonly discover: DiscoveryService;
    readonly reflector: Reflector;
    readonly settings: IGlobalSetting;
    readonly setEndpointsDataCb?: Function | undefined;
    readonly checkHashSumCb?: Function | undefined;
    private readonly logger;
    private readonly policiesData;
    private readonly iamSdkService;
    constructor(discover: DiscoveryService, reflector: Reflector, settings: IGlobalSetting, setEndpointsDataCb?: Function | undefined, checkHashSumCb?: Function | undefined);
    updateEndpointPolicies(): Promise<void>;
    private collectPolicies;
    private getUrl;
    private sortEndpointPoliciesData;
    private checkHashSum;
    private processData;
}
