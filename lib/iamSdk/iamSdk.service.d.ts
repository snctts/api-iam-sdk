import { AxiosInstance } from 'axios';
import { Logger } from '../logger';
import { IPolicyItem } from './iamSdk.interface';
export declare class IamSdkService {
    readonly serviceName: string;
    readonly iamHost: string;
    readonly logger: Logger;
    readonly axios: AxiosInstance;
    private readonly expectedHashSumHttpCodes;
    constructor(serviceName: string, iamHost: string, logger: Logger, axios: AxiosInstance);
    private isAxiosError;
    private logError;
    getControlHashSum(namespace: string): Promise<string | undefined>;
    sendEndpointsData(policies: IPolicyItem[], namespace: string): Promise<void>;
}
