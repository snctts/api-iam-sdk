"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.IamSdkService = void 0;
class IamSdkService {
    constructor(serviceName, iamHost, logger, axios) {
        this.serviceName = serviceName;
        this.iamHost = iamHost;
        this.logger = logger;
        this.axios = axios;
        this.expectedHashSumHttpCodes = [404];
    }
    isAxiosError(error) {
        return error.isAxiosError === true;
    }
    logError(err, expectedHttpCodes) {
        var _a, _b;
        if (this.isAxiosError(err) && ((_a = err.response) === null || _a === void 0 ? void 0 : _a.status)) {
            if (!(expectedHttpCodes === null || expectedHttpCodes === void 0 ? void 0 : expectedHttpCodes.includes((_b = err.response) === null || _b === void 0 ? void 0 : _b.status))) {
                this.logger.httpError(err);
            }
        }
        else {
            this.logger.error(err);
        }
    }
    getControlHashSum(namespace) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const url = `http://${this.iamHost}/api/iam/${namespace}/policies/${this.serviceName}/endpoints/checksum`;
                const response = yield this.axios.get(url);
                return response === null || response === void 0 ? void 0 : response.data;
            }
            catch (err) {
                this.logError(err, this.expectedHashSumHttpCodes);
            }
        });
    }
    sendEndpointsData(policies, namespace) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const url = `http://${this.iamHost}/api/iam/${namespace}/policies/${this.serviceName}/endpoints`;
                yield this.axios.post(url, policies);
            }
            catch (err) {
                this.logError(err);
            }
        });
    }
}
exports.IamSdkService = IamSdkService;
//# sourceMappingURL=iamSdk.service.js.map