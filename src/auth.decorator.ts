import {SetMetadata} from '@nestjs/common';

export const Auth = (...args: any[]) => SetMetadata('auth', args);
