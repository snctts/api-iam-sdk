import {AxiosError} from 'axios';

export class Logger {
  private readonly loggerEnabled: boolean;

  constructor(loggerEnabled: boolean = false) {
    this.loggerEnabled = loggerEnabled;
  }

  debug(message: string) {
    if (!this.loggerEnabled) {
      return;
    }
    // tslint:disable-next-line: no-console
    console.log(message);
  }

  error(error: unknown) {
    // tslint:disable-next-line: no-console
    console.error(error);
  }

  httpError(error: AxiosError) {
    // tslint:disable-next-line: no-console
    console.log(this.simplifyAxiosError(error));
  }

  private tryParseJsonString(value: any): any {
    try {
      const jsonObj = JSON.parse(value);
      return jsonObj;
    } catch (e) {
      return value;
    }
  }

  private simplifyAxiosError(err: AxiosError): any {
    const res = err.response;
    const {stackTrace, ...body} = res?.data ? this.tryParseJsonString(res.data) : res?.data;
    return {
      method: res?.config && res.config.method,
      url: res?.config && res.config.url,
      status: res?.status,
      message: err.message ? err.message : undefined,
      body,
    };
  }
}
