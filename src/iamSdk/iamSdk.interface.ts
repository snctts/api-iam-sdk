export interface IGlobalSetting {
  logProcess?: boolean;
  baseUrl: string;
  serviceName: string;
  iamHost: string;
}

export interface IPolicyItem {
  permissions: string[];
  url: string;
  method: string;
  title: string;
  description: string;
  permanent: boolean;
}

export interface IPolicyData {
  [key: string]: IPolicyItem[];
}
