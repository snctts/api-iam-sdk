import {DiscoveryService} from '@golevelup/nestjs-discovery';
import {Reflector} from '@nestjs/core';
import {RequestMethod, Type} from '@nestjs/common';
import {createHash} from 'crypto';
import {default as axios} from 'axios';

import {IGlobalSetting, IPolicyData, IPolicyItem} from './iamSdk.interface';
import {IamSdkService} from './iamSdk.service';
import {DECORATOR} from '../constants';
import {Logger} from '../logger';

export class IamSdk {
  private readonly logger: Logger;
  private readonly policiesData: IPolicyData = {};
  private readonly iamSdkService: IamSdkService;
  /* tslint:disable */
  constructor(
    readonly discover: DiscoveryService,
    readonly reflector: Reflector,
    readonly settings: IGlobalSetting,
    readonly setEndpointsDataCb?: Function,
    readonly checkHashSumCb?: Function,
  ) {
    this.discover = discover;
    this.logger = new Logger(settings.logProcess);
    this.reflector = reflector;
    this.settings = settings;
    this.iamSdkService = new IamSdkService(
      this.settings.serviceName,
      this.settings.iamHost,
      this.logger,
      axios.create(),
    );
  }
  /* tslint:disable */

  async updateEndpointPolicies(): Promise<void> {
    await this.collectPolicies();
    await this.processData();
  }

  private async collectPolicies(): Promise<void> {
    const decoratedMethods = await this.discover.methodsAndControllerMethodsWithMetaAtKey<any>(DECORATOR);

    this.logger.debug(`Starting collection...`);
    this.logger.debug(`Total decorated methods found: ${decoratedMethods.length}`);

    for (const item of decoratedMethods) {
      const controllerRoute = this.reflector.get(
        'path',
        item.discoveredMethod?.parentClass?.injectType as Function | Type<any>,
      );
      const endpointRoute = this.reflector.get('path', item.discoveredMethod?.handler);
      const endpointMethod = this.reflector.get('method', item.discoveredMethod?.handler);
      const endpointOperation = this.reflector.get('swagger/apiOperation', item.discoveredMethod?.handler);
      const url = this.getUrl(controllerRoute, endpointRoute);

      item.meta[0].forEach((element: any) => {
        if (!this.policiesData[element.namespace]) {
          this.policiesData[element.namespace] = [];
        }

        this.logger.debug(`Url: ${url}`);

        const policyObject: IPolicyItem = {
          permanent: false,
          permissions: element.permissions,
          url,
          method: RequestMethod[endpointMethod],
          title: endpointOperation.operationId,
          description: endpointOperation.description,
        };

        this.policiesData[element.namespace].push(policyObject);
      });
    }
    this.logger.debug(`Existing namespaces: ${Object.keys(this.policiesData)}`);
  }

  private getUrl(controllerRoute: string, endpointRoute: string) {
    const resultUrl = [this.settings.baseUrl];
    if (controllerRoute && controllerRoute.trim() !== '/') {
      resultUrl.push(controllerRoute.trim());
    }

    if (endpointRoute && endpointRoute.trim() !== '/') {
      resultUrl.push(endpointRoute.trim());
    }

    return resultUrl.join('/');
  }

  private sortEndpointPoliciesData(data: IPolicyItem[]): IPolicyItem[] {
    data.forEach((item) => item.permissions.sort());
    return data.sort((a, b) => {
      if (a.url < b.url) {
        return -1;
      }
      if (a.url > b.url) {
        return 1;
      }
      if (a.method < b.method) {
        return -1;
      }
      if (a.method > b.method) {
        return 1;
      }
      return 0;
    });
  }

  private async checkHashSum(namespace: string): Promise<boolean> {
    let controlHashSum;
    if (this.checkHashSumCb) {
      controlHashSum = await this.checkHashSumCb(namespace, this.settings.serviceName);
    } else {
      controlHashSum = await this.iamSdkService.getControlHashSum(namespace);
    }
    const endpoints = this.policiesData[namespace];
    const sortedEndpoints = this.sortEndpointPoliciesData(endpoints);
    const hash = createHash('md5').update(JSON.stringify(sortedEndpoints)).digest('hex');

    this.logger.debug(`Control hashsum: ${controlHashSum}`);
    this.logger.debug(`Current hashsum: ${hash}`);

    return hash === controlHashSum;
  }

  private async processData(): Promise<void> {
    const namespaces = Object.keys(this.policiesData);
    for (const namespace of namespaces) {
      const isDataActual = await this.checkHashSum(namespace);
      if (!isDataActual) {
        this.logger.debug(`Processing data for namespace: ${namespace}`);
        if (this.setEndpointsDataCb) {
          await this.setEndpointsDataCb(namespace, this.settings.serviceName, this.policiesData[namespace]);
          continue;
        }
        await this.iamSdkService.sendEndpointsData(this.policiesData[namespace], namespace);
      }
    }
  }
}
