import {AxiosError, AxiosInstance} from 'axios';

import {Logger} from '../logger';
import {IPolicyItem} from './iamSdk.interface';

export class IamSdkService {
  private readonly expectedHashSumHttpCodes: number[] = [404];

  constructor(
    readonly serviceName: string,
    readonly iamHost: string,
    readonly logger: Logger,
    readonly axios: AxiosInstance,
  ) {}

  private isAxiosError(error: AxiosError | unknown): error is AxiosError {
    return (error as AxiosError).isAxiosError === true;
  }

  private logError(err: unknown, expectedHttpCodes?: number[]) {
    if (this.isAxiosError(err) && err.response?.status) {
      if (!expectedHttpCodes?.includes(err.response?.status)) {
        this.logger.httpError(err);
      }
    } else {
      this.logger.error(err);
    }
  }

  async getControlHashSum(namespace: string): Promise<string | undefined> {
    try {
      const url = `http://${this.iamHost}/api/iam/${namespace}/policies/${this.serviceName}/endpoints/checksum`;
      const response = await this.axios.get(url);
      return response?.data as string;
    } catch (err) {
      this.logError(err, this.expectedHashSumHttpCodes);
    }
  }

  async sendEndpointsData(policies: IPolicyItem[], namespace: string): Promise<void> {
    try {
      const url = `http://${this.iamHost}/api/iam/${namespace}/policies/${this.serviceName}/endpoints`;
      await this.axios.post(url, policies);
    } catch (err) {
      this.logError(err);
    }
  }
}
