import {IamSdk} from '../src/iamSdk/iamSdk';

jest.mock('axios');
jest.mock('../src/iamSdk/iamSdk.service');

const metaMock = [
  {
    discoveredMethod: {handler: jest.fn(), methodName: 'methodName', parentClass: {}},
    meta: [
      [
        {namespace: 'namespace', permissions: ['permission1']},
        {namespace: 'namespace2', permissions: ['permission2']},
      ],
    ],
  },
];

let iamsdk: IamSdk;
let discovery: any;
let reflector: any;
let settings: any;
let checksumMock = jest.fn();
const setEndpointsDataMock = jest.fn();

describe('IamSdk', () => {
  beforeEach(() => {
    discovery = {methodsAndControllerMethodsWithMetaAtKey: jest.fn().mockResolvedValue(metaMock)};
    reflector = {
      get: jest
        .fn()
        .mockReturnValueOnce('vouchers')
        .mockReturnValueOnce('get/code')
        .mockReturnValueOnce(1)
        .mockReturnValueOnce({
          operrationId: 'opId',
          description: 'descr',
        }),
    };
    settings = {baseUrl: 'baseUrl', serviceName: 'serviceName', iamHost: 'host'};
    iamsdk = new IamSdk(discovery, reflector, settings);
  });

  afterEach(() => {
    jest.resetAllMocks();
    jest.clearAllMocks();
  });

  describe('updateEndpointPolicies', () => {
    it('should call class methods ', async () => {
      await iamsdk.updateEndpointPolicies();

      expect(discovery.methodsAndControllerMethodsWithMetaAtKey).toHaveBeenCalled();
      expect(reflector.get).toHaveBeenCalledTimes(4);
    });

    it('should not call 3-rd party methods ', async () => {
      const emptyDiscovery: any = {methodsAndControllerMethodsWithMetaAtKey: jest.fn().mockResolvedValue([])};
      const sdk = new IamSdk(emptyDiscovery, reflector, settings);
      await sdk.updateEndpointPolicies();

      expect(emptyDiscovery.methodsAndControllerMethodsWithMetaAtKey).toHaveBeenCalled();
      expect(reflector.get).toHaveBeenCalledTimes(0);
    });

    it('should call callback methods ', async () => {
      const discoveryInstance: any = {methodsAndControllerMethodsWithMetaAtKey: jest.fn().mockResolvedValue(metaMock)};
      const sdk = new IamSdk(discoveryInstance, reflector, settings, setEndpointsDataMock, checksumMock);
      await sdk.updateEndpointPolicies();

      expect(checksumMock).toHaveBeenCalledTimes(2);
      expect(setEndpointsDataMock).toHaveBeenCalledTimes(2);
    });

    it('should call callback methods with conditions', async () => {
      const checksumMockCb = jest
        .fn()
        .mockResolvedValueOnce('686d3992ca51f6e3c39dc0e2b0b7d4be')
        .mockResolvedValueOnce('sadad');
      const discoveryInstance: any = {methodsAndControllerMethodsWithMetaAtKey: jest.fn().mockResolvedValue(metaMock)};
      const sdk = new IamSdk(discoveryInstance, reflector, settings, setEndpointsDataMock, checksumMockCb);
      await sdk.updateEndpointPolicies();

      // is expected to be called once per namespace (there are 2 namespaces)
      expect(checksumMockCb).toHaveBeenCalledTimes(2);
      // is expected that the first call is skipped because mocked hashsum is equal to the calculated
      expect(setEndpointsDataMock).toHaveBeenCalledTimes(1);
    });
  });
});
