import {IamSdkService} from '../src/iamSdk/iamSdk.service';
import {Logger} from '../src/logger';
import {IPolicyItem} from '../src/iamSdk/iamSdk.interface';

let service: IamSdkService;
let logger: Logger;

const serviceName = 'service',
  iamHost = 'host',
  namespace = 'nmsp',
  hash = 'hash',
  policiesArr = [
    {
      permissions: ['permission1'],
      url: 'url',
      method: 'get',
      title: 'title',
      description: 'description',
    },
  ] as IPolicyItem[],
  axiosError = {isAxiosError: true, response: {status: 500}},
  axios = {
    get: jest.fn(() => Promise.resolve({data: 'sad'})),
    post: jest.fn(),
  };

describe('IamSdkService', () => {
  beforeEach(async () => {
    const loggerInstance = new Logger(true);
    logger = loggerInstance;
    service = new IamSdkService(serviceName, iamHost, loggerInstance, axios as any);
  });

  describe('getControlHashSum', () => {
    it('should return proper value', async () => {
      axios.get.mockResolvedValueOnce({data: hash});
      const resp = await service.getControlHashSum(namespace);

      expect(resp).toBe(hash);
    });

    it('should call logger httpError function', async () => {
      logger.httpError = jest.fn();
      axios.get.mockRejectedValueOnce(axiosError);
      await service.getControlHashSum(namespace);

      expect(logger.httpError).toHaveBeenCalled();
    });

    it('should call logger error function', async () => {
      logger.error = jest.fn();
      axios.get.mockRejectedValueOnce({message: ''});
      const resp = await service.getControlHashSum(namespace);

      expect(logger.error).toHaveBeenCalled();
    });
  });

  describe('sendEndpointsData', () => {
    it('should call logger httpError function', async () => {
      logger.httpError = jest.fn();
      axios.post.mockRejectedValueOnce(axiosError);
      await service.sendEndpointsData(policiesArr, namespace);

      expect(logger.httpError).toHaveBeenCalled();
    });

    it('should call logger error function', async () => {
      logger.error = jest.fn();
      axios.post.mockRejectedValueOnce({message: ''});
      await service.sendEndpointsData(policiesArr, namespace);

      expect(logger.error).toHaveBeenCalled();
    });
  });
});
