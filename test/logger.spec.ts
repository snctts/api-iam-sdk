import {Logger} from '../src/logger';

let logger: Logger;

const axiosError = {
  config: {},
  response: {
    data: {},
    status: 401,
    statusText: 'text',
    headers: 'headers',
    config: {},
  },
  isAxiosError: true,
  toJSON: jest.fn(),
  name: 'sd',
  message: 'testError',
};

describe('Logger', () => {
  beforeEach(() => {
    logger = new Logger(true);

    console.log = jest.fn();
  });

  describe('debug', () => {
    it('should call console.log', () => {
      logger.debug('message');

      expect(console.log).toHaveBeenCalled();
    });

    it('should not call console.log', () => {
      const noLogger = new Logger(false);

      noLogger.debug('message');

      expect(console.log).toHaveBeenCalledTimes(0);
    });
  });

  describe('error', () => {
    it('should call console.error', () => {
      console.error = jest.fn();
      logger.error(new Error('msg'));

      expect(console.error).toHaveBeenCalled();
    });
  });

  describe('httpError', () => {
    it('should call console.log', () => {
      logger.httpError(axiosError as any);

      expect(console.log).toHaveBeenCalled();
    });
  });
});
